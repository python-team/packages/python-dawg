import sys
import pkg_resources

from distutils.command.build import build as Build
from distutils.core import Distribution

build = Build(Distribution())
build.finalize_options()
sys.path[:0] = [build.build_platlib]
sys.exit(pkg_resources.load_entry_point(
    'pytest', 'console_scripts', 'py.test')())
